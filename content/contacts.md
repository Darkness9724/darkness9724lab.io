+++
title = "Контактная информация"
draft = false
+++

Все нижеприведённые ссылки ведут на мои реальные профили. Если вы нашли аккаунт на сайте, который тут не указан, значит или я забыл его добавить, или это какой-то индивидуум, косящий (нарочно или нет) под меня.

* Yggdrasil: [aiaz2foy6hg7av7ps2uvuzinxm.meshname](http://aiaz2foy6hg7av7ps2uvuzinxm.meshname) / [201:9d15:d8f1:cdf0:57ef:96a9:5a65:dbb](http://[201:9d15:d8f1:cdf0:57ef:96a9:5a65:dbb5])
* Matrix: [@darkness9724:matrix.org](https://matrix.to/#/@darkness9724:matrix.org)
* Email:
* * [darkness9724@gmail.com](mailto:darkness9724@gmail.com)
* * [darkness9724@protonmail.com](mailto:darkness9724@protonmail.com)
* * [darkness9724@aiaz2foy6hg7av7ps2uvuzinxm.meshname](mailto:darkness9724@aiaz2foy6hg7av7ps2uvuzinxm.meshname) / [darkness9724@\[201:9d15:d8f1:cdf0:57ef:96a9:5a65:dbb\]](mailto:darkness9724@\[201:9d15:d8f1:cdf0:57ef:96a9:5a65:dbb\])
* Discord: Darkness9724#2536
* Youtube: [Darkness9724](https://www.youtube.com/channel/UCo7fzWCPpWm_DZVDhMZ-bdw)
* ВК: [https://vk.com/darkness9724](https://vk.com/darkness9724)
* Twitter: [@Darkness9724](https://twitter.com/darkness9724)
* Gitlab: [https://gitlab.com/Darknes9724](https://gitlab.com/Darknes9724)
* Pastebin: [Darkness9724](https://pastebin.com/u/Darkness9724)
* MyAnimeList: [Darkness9724](https://myanimelist.net/profile/Darkness9724)
* Ficbook: [Darkness9724](https://ficbook.net/authors/1062179)
* Fanfiction.net: [Darkness9724](https://www.fanfiction.net/~darkness9724)
* AO3: [Darkness9724](https://archiveofourown.org/users/Darkness9724)

P.S. Многие меня часто спрашивают, шо означает `9724` в моём нике. А нишо. Просто рандомная соль, которую я когда-то добавил при регистрации на одном форуме в далёком 2013, так как `Darkness` был занят. С тех пор и повелось.
