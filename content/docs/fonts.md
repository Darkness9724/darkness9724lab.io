+++
title = "Сравнение шрифтов"
draft = false
+++

<div id=whitebox>

Computer Modern Unicode:\
<font face="CMU" size=6>Съешь же ещё этих мягких французских булок да выпей чаю.</font>

Cormorant:\
<font face="Cormorant" size=6>Съешь же ещё этих мягких французских булок да выпей чаю.</font>

DejaVu Serif:\
<font face="DejaVu Serif" size=6>Съешь же ещё этих мягких французских булок да выпей чаю.</font>

Liberation Serif:\
<font face="Liberation Serif" size=6>Съешь же ещё этих мягких французских булок да выпей чаю.</font>

Libertinus Serif:\
<font face="Libertinus Serif" size=6>Съешь же ещё этих мягких французских булок да выпей чаю.</font>

Linux Libertine:\
<font face="Linux Libertine" size=6>Съешь же ещё этих мягких французских булок да выпей чаю.</font>

Nimbus Roman:\
<font face="Nimbus Roman" size=6>Съешь же ещё этих мягких французских булок да выпей чаю.</font>

Noto Serif:\
<font face="Noto Serif" size=6>Съешь же ещё этих мягких французских булок да выпей чаю.</font>

PT Serif:\
<font face="PT Serif" size=6>Съешь же ещё этих мягких французских булок да выпей чаю.</font>

Times New Roman:\
<font face="Times New Roman" size=6>Съешь же ещё этих мягких французских булок да выпей чаю.</font>

XITS:\
<font face="XITS" size=6>Съешь же ещё этих мягких французских булок да выпей чаю.</font>

</div>

[TUG Font Catalogue](https://tug.org/FontCatalogue/)