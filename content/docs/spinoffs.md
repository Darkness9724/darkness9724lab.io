+++
title = "Спин-оффы и моды MGQ"
draft = false

#[taxonomies]
#categories = ["Моды"]
#tags = ["mgq"]
+++
<small>Примечание: Авторы, бросившие свои моды — пидо... плохие люди (хоть я и сам бросал 😅).</small>
<h3>Содержание: <a href="#nscripter">NScripter</a> | <a href="#renpy">Ren'Py</a> | <a href="#rpgm">RPGMaker</a> | <a href="#mods">Моды к другим играм</a> </h3> 
<a name="nscripter"></a>
<h2>Для новеллы на NScripter</h2>
<div align="center">
<figure class="sign">
    <p>MGQ: Remastered</p>
    <p>Автор: lugia19 и Sore Eyes</p>
    <p>Статус: В разработке</p>
    <p>Год создания: 2019</p>
    <p>Ссылка: <a href="https://pastebin.com/7Q1QRZ27">Pastebin</a></p>
    <p>Примечание: HD-версия</p>
    <p>Оценка: 7/10</p>
</figure>
<figure class="sign">
    <p>Rogue's patch set</p>
    <p>Автор: GC345</p>
    <p>Статус: В разработке</p>
    <p>Год создания: 2014</p>
    <p>Ссылка: <a href="https://monstergirlquest.fandom.com/wiki/MGQ3_GC345%27s_patch_testing">Страница на вики</a></p>
    <p>Примечание: Набор патчей для NScripter. Разные багфиксы и улучшайзеры.</p>
    <p>Оценка: 8/10</p>
</figure>
<figure class="sign">
    <p>Granberia &amp; Tamamo: To the Coliseum!</p>
    <p>Автор: Ecstasy</p>
    <p>Статус: Брошен</p>
    <p>Год создания: 2013</p>
    <p>Ссылка: <a href="https://monstergirlquest.fandom.com/wiki/Granberia_%26_Tamamo:_To_the_Coliseum!">Страница на вики</a></p>
    <p>Примечание: Кривая хрень. Хорошо хоть, что до юри не дошло</p>
    <p>Оценка: 4/10</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: NG+</p>
    <p>Автор: Ecstasy</p>
    <p>Статус: В разработке</p>
    <p>Год создания: 2013</p>
    <p>Ссылка: <a href="https://ecstasywastaken.blogspot.com/">Блог</a></p>
    <p>Примечание: ИМХО, лучший мод</p>
    <p>Оценка: 9/10</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: NG+</p>
    <p>Автор: SpasMaran</p>
    <p>Статус: Брошен</p>
    <p>Год создания: 2012</p>
    <p>Ссылка: <a href="https://monstergirlquest.fandom.com/wiki/Monster_Girl_Quest:_NG%2B_(SpasMaran)#Download_Link">Страница вики</a></p>
    <p>Примечание: Сюжета как такового нет. Переделаны лишь бои.</p>
    <p>Оценка: 6/10</p>
</figure>
<figure class="sign">
    <p>Poke Monster Girl Quest</p>
    <p>Автор: ???</p>
    <p>Язык: Английский</p>
    <p>Статус: Брошен</p>
    <p>Год создания: 2013</p>
    <p>Ссылка: <a href="https://monstergirlquest.fandom.com/wiki/Poke_Monster_Girl_Quest">Страница на вики</a></p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: Sealed</p>
    <p>Автор: Ginrikuzuma</p>
    <p>Статус: Брошен</p>
    <p>Год создания: 2012</p>
    <p>Ссылка: <a href="https://monstergirlquest.fandom.com/wiki/Sealed#Download_and_Current_Tasks">Страница на вики</a></p>
    <p>Примечание: Упоротая, но забавная хрень</p>
    <p>Оценка: 7/10</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: War of The Goddesses</p>
    <p>Автор: c0var1ant</p>
    <p>Язык: Английский</p>
    <p>Статус: Брошен, нет публичной версии</p>
    <p>Год создания: 2012</p>
    <p>Ссылка: <a href="https://monstergirlquest.fandom.com/wiki/War_of_the_Goddesses#Download_.28OLD_ENGINE.29_-_Stable_Episode_Releases">Страница на вики</a>, все опубликованные ссылки просрочены</p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: Servant of the Monster Lord</p>
    <p>Автор: Zepekinio25</p>
    <p>Язык: Английский</p>
    <p>Статус: В разработке</p>
    <p>Год создания: 2014</p>
    <p>Ссылка: <a href="https://mgqservantofthemonsterlord.blogspot.fr/">Блог</a></p>
    <p>Примечание: Луку заменили на какого-то Мэри Сью. В целом ничё.</p>
    <p>Оценка: 5/10</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: Alpha</p>
    <p>Автор: Miyame</p>
    <p>Язык: Английский</p>
    <p>Статус: Брошен, нет публичной версии</p>
    <p>Год создания: 2014</p>
    <p>Ссылка: <a href="https://monstergirlquest.fandom.com/wiki/MGQ:_Alpha_-_Report_Page">Багтрекер</a>, скорее всего не была публично опубликована</p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: Lost Chapter</p>
    <p>Автор: Nosaki</p>
    <p>Язык: Испанский</p>
    <p>Статус: Брошен, нет публичной версии</p>
    <p>Год создания: 2016</p>
    <p>Ссылка: <a href="https://nosakiproyectsovn.blogspot.com/">Блог</a>, все опубликованные ссылки просрочены</p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: Party Edition</p>
    <p>Автор: ???</p>
    <p>Язык: Английский</p>
    <p>Статус: Брошено, нет публичной версии</p>
    <p>Год создания: 2012</p>
    <p>Ссылка: <a href="https://gamefaqs.gamespot.com/boards/646357-monmusu-quest/63711780">Форум</a>, автор заблокирован, все опубликованные ссылки просрочены</p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Path of a Hero</p>
    <p>Автор: Lance Ivy</p>
    <p>Язык: Английский</p>
    <p>Статус: Брошено, нет публичной версии</p>
    <p>Год создания: 2013</p>
    <p>Ссылка: Утеряна</p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: Dimensions</p>
    <p>Автор: MasterKitsune</p>
    <p>Язык: Английский</p>
    <p>Статус: Брошено, нет публичной версии</p>
    <p>Год создания: 2014</p>
    <p>Ссылка: <a href="https://monstergirlquest.fandom.com/wiki/Monster_Girl_Quest:_Dimensions">Страница на вики</a>, ссылок нет</p>
    <p>Примечание: Не знаю чё там по сюжету должно было быть, но Илиас-гот доставляет</p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Doki Doki Monster Club</p>
    <p>Автор: Doki Doki Monster Club</p>
    <p>Язык: Английский</p>
    <p>Статус: Брошено, нет публичной версии</p>
    <p>Год создания: 2018</p>
    <p>Ссылка: <a href="https://monstergirlquest.fandom.com/wiki/Doki_doki_monster_club">Страница на вики</a>, ссылка просрочена. Однако в своё время мне удалось <b>это</b> скачать (вроде даже осталась локальная копия).</p>
    <p>Примечание: Куча гармонических ашибак. Глаза вытекают.</p>
    <p>Оценка: 1/10</p>
</figure>
<figure class="sign">
    <p>Saito Adventure</p>
    <p>Автор: Bima1001</p>
    <p>Язык: Английский</p>
    <p>Статус: Брошено, нет публичной версии</p>
    <p>Год создания: 2013</p>
    <p>Ссылка: <a href="https://monstergirlquest.fandom.com/wiki/Saito_Adventures">Страница на вики</a></p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p><a href="https://ux.getuploader.com/mon_mod/">Коллекция японских модов</a></p>
    <p><a href="https://www.kyodemo.net/sdemo/b/s_otaku_17194/?j">Ещё одна коллекция</a></p>
</figure>
</div>
<a name="renpy"></a>
<h2>Ren'Py</h2>
<div align="center">
<figure class="sign">
    <p>Kirito Adventure</p>
    <p>Автор: Neosss</p>
    <p>Язык: Испанский</p>
    <p>Статус: Завершено/Брошено</p>
    <p>Год создания: 2015?</p>
    <p>Ссылка: <a href="https://github.com/NeosssSoft/Monster-Girl-Quest-Kirito-Adventure">Github</a></p>
    <p>Примечание: В связи с незнанием испанского не могу сказать каков статус. Скорее всего завершён.</p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: New Adventure</p>
    <p>Автор: Darkness9724</p>
    <p>Язык: Английский/Русский</p>
    <p>Статус: Брошено, нет публичной версии</p>
    <p>Год создания: 2015</p>
    <p>Ссылка: Утеряна, хоть и была опубликована на dropbox</p>
    <p>Примечание: Сюжет - говно</p>
    <p>Оценка: 1/10</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: NG+</p>
    <p>Автор: Darkness9724</p>
    <p>Язык: Русский</p>
    <p>Статус: В разработке</p>
    <p>Год создания: 2018</p>
    <p>Ссылка: <a href="https://gitlab.com/Darkness9724/mgq-ng-ru">Gitlab</a></p>
    <p>Примечание: Перевод англоязычной версии</p>
    <p>Оценка: N/A</p>
</figure>
</div>
<a name="rpgm"></a>
<h2>RPGMaker</h2>
<div align="center">
<figure class="sign">
    <p>Monster Girl Quest: WasoN</p>
    <p>Автор: netto-painter</p>
    <p>Язык: Английский</p>
    <p>Статус: Брошен, нет публичной версии</p>
    <p>Год создания: 2017</p>
    <p>Ссылка: <a href="https://www.deviantart.com/netto-painter/journal/MGQ-WasoN-WIP-Fangame-703687283">DevianArt</a></p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: Heaven's Utopia</p>
    <p>Автор: luigitheman20</p>
    <p>Язык: Английский</p>
    <p>Статус: Брошена, нет публичной версии</p>
    <p>Год создания: 2016</p>
    <p>Ссылка: <a href="https://www.deviantart.com/luigitheman20/journal/Monster-Girl-Quest-Heaven-s-Utopia-Sound-Test-632551131">DevianArt</a></p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Normal Plus</p>
    <p>Автор: Brandon Kline</p>
    <p>Язык: Английский</p>
    <p>Статус: Завершено</p>
    <p>Год создания: 2019</p>
    <p>Ссылка: <a href="https://drive.google.com/file/d/1nVd_hAzanqyQtJNofTyGlFc1vRXxDj3v/edit">Гугл диск</a></p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Black Alice is the Best Girl</p>
    <p>Автор: ???</p>
    <p>Язык: Английский</p>
    <p>Статус: Брошено, нет публичной версии</p>
    <p>Год создания: 2019</p>
    <p>Ссылка: <a href="https://drive.google.com/file/d/1x-j8yebEQOPZzunVuvKtyu5V1p0T_9cY/view">Гугл диск</a>, ссылка просрочена</p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: Paradox EX</p>
    <p>Автор: MGQ EX</p>
    <p>Язык: Английский</p>
    <p>Статус: Брошено</p>
    <p>Год создания: 2015</p>
    <p>Ссылка: <a href="https://mgq-ex.blogspot.com/">Блок</a></p>
    <p>Примечание: Улучшайзеры геймплея, в основном сюжетки</p>
    <p>Оценка: 6/10</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: Paradox EX</p>
    <p>Автор: Data Corrupted</p>
    <p>Язык: Русский</p>
    <p>Статус: Завершено</p>
    <p>Год создания: 2018</p>
    <p>Ссылка: <a href="https://vk.com/topic-78706918_38727044">ВК</a></p>
    <p>Примечание: Улучшайзеры геймплея, в основном ЛХ</p>
    <p>Оценка: 7/10</p>
</figure>
<figure class="sign">
    <p>Hyperdimension Girl Quest</p>
    <p>Автор: HeroLuka</p>
    <p>Язык: Английский</p>
    <p>Статус: В разработке</p>
    <p>Год создания: 2018</p>
    <p>Ссылка: <a href="https://monstergirlquest.fandom.com/wiki/Hyperdimension_Girl_Quest!">Страница </a></p>
    <p>Примечание: Мод чуть хуже NG+, но всё равно на уровне</p>
    <p>Оценка: 8/10</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: Remake</p>
    <p>Автор: Unknown Developer</p>
    <p>Язык: Английский</p>
    <p>Статус: В разработке</p>
    <p>Год создания: 2018</p>
    <p>Ссылка: <a href="https://f95zone.to/threads/monster-girl-quest-fan-remake-v0-21-unknown-developer.11499/">Форум</a></p>
    <p>Примечание: Вполне неплох, однако некоторые моменты убивают (например Наоми вместо Нанаби)</p>
    <p>Оценка: 7/10</p>
</figure>
<figure class="sign">
    <p>Scott's Big Adventure</p>
    <p>Автор: RPMaestro</p>
    <p>Язык: Английский</p>
    <p>Статус: Завершено</p>
    <p>Год создания: 2019</p>
    <p>Ссылка: <a href="https://mega.nz/#!WtExgCYI!BhtQ5DFlkUbHp-HhTtynL4YPPXrqnWdi6Yrv4t2S0T0">MEGA</a></p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Maestro's Monmusu</p>
    <p>Автор: RPMaestro</p>
    <p>Язык: Английский</p>
    <p>Статус: В разработке</p>
    <p>Год создания: 2019</p>
    <p>Ссылка: <a href="https://mega.nz/#!P1kVCAhS!9BMAk3UdkUBOGQ8EoIHjeQunvhWBUdZfjgMtmVS2-xk">MEGA</a>, версия 1.02</p>
    <p>Примечание: Версия 1.10 доступна лишь за деньги на Patreon. Что так-то незаконно.</p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Frederika's Bizarre Quest</p>
    <p>Автор: Doctor Gigawatt</p>
    <p>Язык: Русский</p>
    <p>Статус: В разработке, нет публичной версии</p>
    <p>Год создания: 2018</p>
    <p>Ссылка: Нулевая глава более недоступна</p>
    <p>Оценка: 6/10</p>
</figure>   
<figure class="sign">
    <p>Monster Girl Quest: AD</p>
    <p>Автор: Darkness9724</p>
    <p>Язык: Английский</p>
    <p>Статус: Брошено, нет публичной версии</p>
    <p>Год создания: 2016</p>
    <p>Примечание: Сюжет был неплох, однако мод помножился на ноль из-за винды</p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest: After Story</p>
    <p>Автор: Darkness9724</p>
    <p>Язык: Русский</p>
    <p>Статус: В разработке, нет публичной версии</p>
    <p>Год создания: 2018</p>
    <p>Оценка: N/A</p>
</figure>
</div>
<a name="mods"></a>
<h2>Моды к другим играм</h2>
<div align="center">
<figure class="sign">
    <p>Open MGQ</p>
    <p>Для игры: движок Unity</p>
    <p>Автор: eternal</p>
    <p>Язык: Английский</p>
    <p>Статус: Брошена</p>
    <p>Ссылка: <a href="https://gitlab.com/censn/open-mgq">Gitlab</a></p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest 3D</p>
    <p>Для игры: Doom</p>
    <p>Автор: HDoom</p>
    <p>Язык: Английский</p>
    <p>Статус: В разработке</p>
    <p>Ссылка: <a href="https://monstergirlquest.fandom.com/wiki/Monster_girl_quest_3d">Страница на вики</a>, демо</p>
    <p>Примечание: Как я понял, полная версия доступна лишь за деньги на Patreon. Что так-то незаконно.</p>
    <p>Оценка: 5/10, только за неплохие спрайты</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest!</p>
    <p>Для игры: Stellaris</p>
    <p>Автор: GhostPhanom</p>
    <p>Язык: N/A</p>
    <p>Статус: В разработке</p>
    <p>Ссылка: <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=1500056151">Мастерская Steam</a></p>
    <p>Примечание: Нет разделения по расам, из-за чего играя за 
"расу" кицунэ у тебя в колониях могут жить ангелы. В принципе, может это
и плюс.</p>
    <p>Оценка: 5/10</p>
</figure>
<figure class="sign">
    <p>Monmusu Quest! The Undead Girls's Rise!</p>
    <p>Для игры: Left 4 Dead</p>
    <p>Автор: TENKO MiKO</p>
    <p>Язык: Английский</p>
    <p>Статус: Завершено</p>
    <p>Ссылка: <a href="https://steamcommunity.com/workshop/filedetails/?id=1217282345">Мастерская Steam</a></p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p><a href="https://osu.ppy.sh/beatmapsets?q=monster%20girl%20quest&amp;s=7">Коллекция песен для osu!</a> (нужна регистрация)</p>
</figure>
<figure class="sign">
    <p>Лука</p>
    <p>Для игры: движок Mugen</p>
    <p>Автор: Nicobee</p>
    <p>Язык: N/A</p>
    <p>Статус: Брошено</p>
    <p>Ссылка: <a href="https://mugenguild.com/forum/topics/nicobees-wips-luka-monmusu-quest-149039.0.html">Форум</a></p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Карточки Monster Girl Quest</p>
    <p>Для игры: Tabletop Simulator</p>
    <p>Автор: Aelphais</p>
    <p>Язык: Английский</p>
    <p>Статус: Завершено</p>
    <p>Ссылка: <a href="https://www.reddit.com/r/tabletopsimulator/comments/2xbtxb/nsfw_monster_girl_quest_cards_for_tabletop/">Reddit</a></p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Tamamo (Queen of Kitsunes!)</p>
    <p>Для игры: Don't Starve</p>
    <p>Автор: Arcade и Silhh</p>
    <p>Язык: Английский</p>
    <p>Статус: Завершено</p>
    <p>Ссылка: <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=399799824">Мастерская Steam</a></p>
    <p>Оценка: 7/10</p>
</figure>
<figure class="sign">
    <p>Monster Girl Quest цивилизации</p>
    <p>Для игры: Civilization V</p>
    <p>Автор: Jeff The Thunder Man</p>
    <p>Язык: Английский</p>
    <p>Статус: Завершено</p>
    <p>Ссылка: <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=1140082641">Альма</a>, <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=782902505">Алиса</a>, <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=1089824623">Тамамо</a>, <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=786081093">Илиас</a>, <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=1123531064">Искатели истины</a>,</p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Лука, Алиса, Илиас</p>
    <p>Для игры: Fire Pro Wrestling World</p>
    <p>Автор: Bristol и Hellspaz</p>
    <p>Язык: N/A</p>
    <p>Статус: Завершено</p>
    <p>Ссылка: <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=1270158581">Лука</a>, <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=1340161397">Алиса</a>, <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=1331200067">Илиас</a></p>
    <p>Примечание: Выглядит стрёмно, особенно Алиса (впрочем, ничего нового)</p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Обои MGQ</p>
    <p>Для игры: приложение Wallpaper Engine</p>
    <p>Автор: Megganonava, Hornsby #PFR#, Ace Star и Sylph</p>
    <p>Язык: N/A</p>
    <p>Статус: Завершено</p>
    <p>Ссылка: <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=946193546">Лука и Алиса</a>, <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=1840615706">Атака Ангелов</a>, <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=1841269959">Алиса</a>, <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=1458933315">Богиня Хаоса</a></p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Лука и Алиса</p>
    <p>Для игры: Crypt of the NecroDancer</p>
    <p>Автор: CanzarSlime</p>
    <p>Язык: N/A</p>
    <p>Статус: Завершено</p>
    <p>Ссылка: <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=354683471">Мастерская Steam</a></p>
    <p>Оценка: N/A</p>
</figure>
<figure class="sign">
    <p>Тамамо</p>
    <p>Для игры: программа FaceRig</p>
    <p>Автор: Takatalvi</p>
    <p>Язык: N/A</p>
    <p>Статус: Завершено</p>
    <p>Ссылка: <a href="https://steamcommunity.com/sharedfiles/filedetails/?id=936953135">Мастерская Steam</a></p>
    <p>Оценка: 8/10</p>
</figure>
<figure class="sign">
    <p>Тамамо</p>
    <p>Для игры: программа Shimeji</p>
    <p>Автора: Cachomon</p>
    <p>Язык: N/A</p>
    <p>Статус: Завершено</p>
    <p>Ссылка: <a href="https://www.deviantart.com/cachomon/art/Tamamo-Shimeji-FREE-694317059">DeviantArt</a></p>
    <p>Оценка: 7/10</p>
</figure>
</div>
