+++
title = "Список и описание пакетов CTAN"
draft = false

#[taxonomies]
#tags = ["tex"]
+++

Т.к я работаю в основном с LuaTeX, пакеты будут описываться для неё.

# Компиляторы документов
* TeX - оригинальная типографическая система, созданная [Дональдом Кнутом](https://www-cs-faculty.stanford.edu/~knuth/) ещё в конце 70-ых годов прошлого века. Бородатая и низкоуровневая система, предоставляющая только команды сдвига и смены шрифта, что делает её похожей на PostScript. В связи с этой сложностью вокруг низкоуровневого ядра были написаны обвязки в виде макросов. Наиболее популярные и живые: LaTeX, ConTeXt и AMS-TeX. Они позволяют писать типографию на профессиональном уровне даже любителю. Компилирует в DVI, поэтому для получения PDF нужно сначала преобразовать получившийся файл в PS, а его в PDF (например, через Ghostscript). Понимает шрифты только в формате Type1 и <font face="METAFONT">METAFONT</font>.
* [pdfTeX](https://tug.org/pdftex/) - улучшенная версия компилятора  <s>e</s>TeX, написанная [Хан Тхе Тханем](https://tug.org/interviews/thanh.html) в начале нулевых. Позволяет компилировать документы напрямую в PDF, минуя цепочку преобразований DVI→PS→PDF. Это расширяет типографические возможности (например, появляется поддержка микротипографики в виде висячей пунктуация и незаметном растяжении шрифта), расширяет возможности использования перекрёстных ссылок и графики (см. пакеты hyperref и PGF/TikZ ниже), а также ускоряет процесс построения итогового документа.
* [XeTeX](https://tug.org/xetex/) - юникод-ориентированный компилятор TeX, ожидающий на входе кодировку UTF-8. Был написан [Джонатаном Кью](https://tug.org/interviews/kew.html) в 2004 году. Позволяет использовать системные шрифты в формате TrueType/OpenType и предоставляет дополнительный функционал в этом деле. Однако, подобно чистому TeX, использует цепочку преобразования DVI→PS→PDF (xdvipdfmx), что из микротипографических возможностей позволило реализовать только висячую пунктуацию.
* [LuaTeX](http://luatex.org) - юникод-ориентированная, подобно XeTeX, система TeX, а также слоупочный интерпретатор Lua. Разработка началась в 2007 году командой под руководством [Тако Хокуотера](https://tug.org/interviews/hoekwater.html). Движок был принят командой разработчиков pdfTeX как их идейный наследник. Вобрал в себя как плюсы pdfTeX (прямое преобразование документа в PDF и микротипографика), так и XeTeX (поддержка юникода и системных шрифтов в формате TrueType/OpenType). В качестве альтернативы языка TeX предоставляет язык Lua, которому доступны все внутренние механизмы движка. Однако, несмотря на все плюсы, работает заметно медленее и потребляет больше памяти, чем XeTeX и pdfTeX (в 1.5 и 6 раз, соответственно).

# Утилиты
* [Ghostscript](http://https://www.ghostscript.com) - интерпретатор [PostScript](https://ru.wikipedia.org/wiki/PostScript) и набор утилит для работы с ps и pdf.
* [Poppler](http://https://poppler.freedesktop.org) - библиотека рендеринга и набор консольных утилит для работы с pdf.
* [qpdf](http://qpdf.sourceforge.net) - свободная реализация фич [Adobe Acrobat](https://ru.wikipedia.org/wiki/Adobe_Acrobat), в часности оптимизация для веба, шифрование и сжатие.
* [gImageReader](https://github.com/manisandro/gImageReader) - фронтэнд к [Tesseract](https://ru.wikipedia.org/wiki/Tesseract) [OCR](https://ru.wikipedia.org/wiki/OCR) для оцифровки и распознования текста отсканированных страниц.
* [pdftk](https://gitlab.com/pdftk-java/pdftk) - ещё одна утилита для работы с PDF. Удобна для нарезки страниц. Написана на Java и тянет её рантайм.
* [DiffPDF](https://gitlab.com/eang/diffpdf) - удобное сравнение страниц PDF.
* [optpdf](https://gitlab.com/Darkness9724/optpdf) - моя кривая утилита, максимально упрощающая сжатие pdf. Использует библиотеки Ghostscript и qpdf.

# Классы документа
* article, book, report и letter - стандартные классы, устаревшие ещё когда динозавры жили на земле. Реализуют статью, книгу, отчёт и письмо, соответственно. Используют много костылей и подпорок, чтобы сохранять совместимость с производными форматами. Не желателен, особенно с современными компиляторами TeX (LuaTeX, XeTeX).
* {{ctan(pkg="koma-script", doc="http://mirrors.ctan.org/macros/latex/contrib/koma-script/doc/scrguien.pdf" desc="отличная замена базовым классам. Аналогами вышеперечисленных классов являются scrartcl, scrreprt, scrbook и scrlttr2.", img=1)}}
* minimal - класс, используемый когда важно быстро скомпилировать содержимое документа, наплевав на стиль (например, для черновика).
* sliders - класс для создания презентаций (слайдов). Такой же древний, как и первые классы. И такой же нежелательный.
* beamer - современная замена классу выше. Предоставляет куда больше фич.
{{image(src="https://www.ctan.org/teaser/pkg/beamer")}}
* hepthesis - класс для академический отчётов, особенно для подготовки диссертаций в области физики.
* moderncv - класс для оформления резюме в соответствии с современными стандартами дизайна.
{{image(src="https://www.ctan.org/teaser/pkg/moderncv")}}
* papertex - вёрстка газет. Не очень удобна, но вменяемых альтернатив нет.
{{image(src="https://www.ctan.org/teaser/pkg/papertex")}}

# Язык и шрифты
* {{ctan(pkg="babel", doc="http://mirror.macomnet.net/pub/CTAN/macros/latex/required/babel/base/babel.pdf" desc="пакет локализации документов LaTeX. Создавался для pdfTeX, что, впрочем, не мешает его использовать с другими компиляторами. Поддерживает около 200 языков.")}}
* {{ctan(pkg="polyglossia", doc="http://mirrors.mi.ras.ru/CTAN/macros/unicodetex/latex/polyglossia/polyglossia.pdf" desc="пакет локализации документов LaTeX. Пришёл на смену пакету babel для Xetex и LuaTeX. Поддерживает более семидесяти языков без учёта диалектов. Не работает с некоторыми старыми стилями.")}}
* {{ctan(pkg="fontspec", doc="http://mirror.macomnet.net/pub/CTAN/macros/latex/contrib/fontspec/fontspec.pdf", desc="предоставляет команды загрузки и смены TrueType/OpenType шрифтов и лигатур. Для поддержки японского нужно также загрузить luatexja, luatexja-fontspec и luatexja-ruby (последнее нужно для фуриганы)")}}.
* {{ctan(pkg="xcolor", doc="http://mirrors.ctan.org/macros/latex/contrib/xcolor/xcolor.pdf", desc="позволяет менять цвет текста.")}}
* {{ctan(pkg="microtype", doc="http://mirror.macomnet.net/pub/CTAN/macros/latex/contrib/microtype/microtype.pdf", desc="реализует висячую пунктуацию, кернинг и трейсинг для равномерного распределения текста, что улучшает читаемость.")}}
* {{ctan(pkg="anyfontsize", doc="http://mirrors.ctan.org/macros/latex/contrib/anyfontsize/anyfontsize.pdf" desc="позволяет указывать любой желаемый размер шрифта.")}}

# Формулы
* {{ctan(pkg="amsmath", doc="http://mirrors.ctan.org/macros/latex/required/amsmath/amsldoc.pdf", desc="добавляет и улучшает возможности математических выражений. Основывается на наборе макросов AMS-TeX к ядру TeX.")}}
* {{ctan(pkg="unicode-math", doc="http://mirrors.ctan.org/macros/latex/contrib/unicode-math/unicode-math.pdf", desc="позволяет использовать символы юникода в формулах.")}}
* {{ctan(pkg="siunitx", doc="http://mirror.macomnet.net/pub/CTAN/macros/latex/contrib/siunitx/siunitx.pdf", desc="пакет с единицами измерений, оформленных по правилам СИ. Также добавляет множество символов всех сортов и расцветок.")}}

# Гиперссылки
* {{ctan(pkg="bookmark", doc="http://mirrors.ctan.org/macros/latex/contrib/bookmark/bookmark.pdf", desc="автоматически индексирует закладки при использовании hyperref, что позволяет избежать второго вызова компилятора.")}}
* {{ctan(pkg="hyperref", doc="http://mirrors.ctan.org/macros/latex/contrib/hyperref/doc/manual.pdf", desc="реализует механизм гиперссылок, улучшает перекрёсные ссылки и создаёт оглавние для навигации по PDF.")}}

# Оформление
* {{ctan(pkg="float", desc="расширяет возможность по позиционированию плавающих объектов (таблиц, изображений, etc.). В частности позволяет указывать абсолютную позицию.")}}
* {{ctan(pkg="scrlayer", desc="аналог пакета fancyhdr для работы с колонтитулами в среде KOMA-Script.")}}

# PGF/TikZ
* {{ctan(pkg="tikz", doc="http://cremeronline.com/LaTeX/minimaltikz.pdf", desc="пакет, предоставляющий удобный интерфейс к pgf, что облегчает рисование сложных фигур. Приходится на замену pstricks и metapost.")}}
* {{ctan(pkg="circuitikz", doc="http://mirrors.ctan.org/graphics/pgf/contrib/circuitikz/doc/circuitikzmanual.pdf", desc="предоставляет макросы, упрощающие рисование электрических схем.", img=1)}}
* {{ctan(pkg="tikz-planets", doc="http://ctan.altspu.ru/graphics/pgf/contrib/tikz-planets/planets-doc.pdf", desc="примитивы для рисования небесных тел одной командой.")}}
