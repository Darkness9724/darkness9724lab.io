+++
title = "Переменные окружения SDL2"
draft = false

#[taxonomies]
#tags = ["sdl"]
+++ 
Проверено на SDL2 2.0.14.

## Видео
Ввод-вывод графики.
#### SDL_VIDEODRIVER
`src/video/SDL_sysvideo.h`
###### Linux и *BSD
Значение | Описание 
---------|----------
x11      | Использовать X Window System. [🔗](https://en.wikipedia.org/wiki/X_Window_System)
directfb | Использовать DirectFB API. [🔗](https://en.wikipedia.org/wiki/DirectFB)
android  | Использовать Android NDK для Android. [🔗](https://en.wikipedia.org/wiki/Android_NDK)
rpi      | Использовать Broadcom GPU API для Rasberry Pi. [🔗](https://en.wikipedia.org/wiki/Raspberry_Pi#Driver_APIs)
kmsdrm   | Использовать нативный KMS/DRM API ядра Linux. [🔗](https://en.wikipedia.org/wiki/Direct_Rendering_Manager)
wayland  | Использовать Wayland. [🔗](https://en.wikipedia.org/wiki/Wayland)

###### Windows
Значение | Описание 
---------|----------
windows  | Использовать нативный Win32 API для Windows NT. [🔗](https://en.wikipedia.org/wiki/Windows_API)
winrt    | Использовать объектно-ориентированный Windows Runtime API для Windows NT. [🔗](https://en.wikipedia.org/wiki/C++/WinRT)

###### macOS
Значение | Описание 
---------|----------
cocoa    | Использовать нативный API macOS. [🔗](https://en.wikipedia.org/wiki/Cocoa_(API))
x11      | Использовать X Window System. [🔗](https://en.wikipedia.org/wiki/X_Window_System)
uikit    | Использовать Cocoa Touch API для iOS. [🔗](https://en.wikipedia.org/wiki/Cocoa_Touch)

###### Прочие
Значение | Описание 
---------|----------
haiku    | Использовать нативный API Haiku. [🔗](https://en.wikipedia.org/wiki/BeOS_API)
pandora  | Использовать нативный API консолей Pandora. [🔗](https://en.wikipedia.org/wiki/Pandora_(console))
psp      | Использовать нативный API PlayStation Portable. [🔗](https://en.wikipedia.org/wiki/PlayStation_Portable)
nacl     | Использовать Native Client. [🔗](https://en.wikipedia.org/wiki/Google_Native_Client)
vivante  | Использовать Vivante GPU API. [🔗](https://en.wikipedia.org/wiki/Vivante_Corporation)
qnx | Использовать нативный API QNX. [🔗](https://en.wikipedia.org/wiki/QNX)

###### Псевдо-устройства
Значение | Описание 
---------|----------
<a title="Эй, кукла. Ты задержан или просто глупый?">dummy</a> | Отключить использование графического API[.](https://youtu.be/tZuUNMwWhOU?t=61)
emscripten | Использовать Emscripten для браузера. [🔗](https://en.wikipedia.org/wiki/Emscripten)
offscreen | Рендерить графику без вывода на монитор. [🔗](https://github.com/libsdl-org/SDL/commit/68985371a071b97b961df56d040fbb5ca6493cf3)

#### SDL_OPENGL_LIBRARY
Путь к библиотеке OpenGL. По-умолчанию:
* `/usr/lib/libGL.so` для *nix.
* `%WINDIR%\system32\opengl32.dll` для Windows.

#### SDL_OPENGLES_LIBRARY
Путь к библиотеке OpenGL ES. По-умолчанию:
* `/usr/lib/libGLESv2.so` для *nix.
* В Windows поддержка OpenGL ES отсутствует. Необходимо использование строннего транслятора [ANGLE](https://opensource.google.com/projects/angle). 

#### SDL_VULKAN_LIBRARY
Путь к библиотеке Vulkan. По-умолчанию:
* `/usr/lib/libvulkan.so` для *nix.
* Для Windows зависит от поставщика драйвера.

#### SDL_VIDEO_X11_VISUALID
ID визуального элемента X11. Переопределяет значение, данное SDL.

#### SDL_VIDEO_X11_LEGACY_FULLSCREEN
Устаревший способ переключения режима окна. Только для X11.

#### SDL_VIDEO_X11_WMCLASS
Имя и класс для свойства WM_CLASS X11. Переопределяет значение по умолчанию.

#### SDL_VIDEO_X11_NODIRECTCOLOR
Если true, не пытаться использовать DirectColor даже если он поддерживается X-сервер (SDL всё ещё будет использовать его для гамма-коррекции). Нужно для старых X-серверов при использовании расширения XVideo.

#### SDL_X11_XCB_LIBRARY
Путь к библиотеке XCB. По-умолчанию `/usr/lib/libxcb.so`.

#### SDL_VIDEO_WAYLAND_WMCLASS
Имя и класс для свойства WM_CLASS Wayland. Переопределяет значение по умолчанию.

#### SDL_VULKAN_DISPLAY
ID дисплея Vulkan.

#### SDL_VIDEO_DUMMY_SAVE_FRAMES
Включает сохранение фреймов в bmp-файлы при запуске с `SDL_VIDEODRIVER=dummy`.

#### SDL_VIDEO_Emscripten_SAVE_FRAMES
Включает сохранение фреймов в bmp-файлы при запуске с `SDL_VIDEODRIVER=emscripten`.

#### SDL_VIDEO_OFFSCREEN_SAVE_FRAMES
Включает сохранение фреймов в bmp-файлы при запуске с `SDL_VIDEODRIVER=offscreen`.

## Аудио
Ввод-вывод звука.
#### SDL_AUDIODRIVER
`src/audio/SDL_sysaudio.h`
###### *nix
Значение | Описание 
---------|----------
pulseaudio | Использовать PulseAudio API. [🔗](https://en.wikipedia.org/wiki/PulseAudio)
alsa     | Использовать нативный Advanced Linux Sound Architecture API для Linux. [🔗](https://en.wikipedia.org/wiki/Advanced_Linux_Sound_Architecture)
jack     | Использовать JACK Audio Connection Kit API. [🔗](https://en.wikipedia.org/wiki/JACK_Audio_Connection_Kit)
sndio    | Использовать нативный sndio API для OpenBSD. [🔗](https://en.wikipedia.org/wiki/sndio)
netbsdaudio | Использовать нативный API NetBSD. [🔗](https://en.wikipedia.org/wiki/C++/WinRT)
dsp      | Использовать Open Sound System API. [🔗](https://en.wikipedia.org/wiki/Open_Sound_System)
qsa      | Использовать нативный QNX Sound Architecture API. [🔗](https://en.wikipedia.org/wiki/QNX)
sun      | Использовать нативный API SunOS/Solaris. [🔗](https://en.wikipedia.org/wiki/SunOS)
arts     | Использовать aRts API. [🔗](https://en.wikipedia.org/wiki/aRts)
esd      | Использовать Enlightened Sound Daemon API. [🔗](https://en.wikipedia.org/wiki/Enlightened_Sound_Daemon)
nas      | Использовать Network Audio System API. [🔗](https://en.wikipedia.org/wiki/Network_Audio_System)
coreaudio| Использовать нативный API macOS и iOS. [🔗](https://en.wikipedia.org/wiki/Core_Audio)
fusionsound | Использовать FusionSound (часть DirectFB) API. [🔗](https://github.com/DirectFB/directfb/tree/master/lib/fusionsound)
androidaudio  | Использовать Android NDK для Android. [🔗](https://en.wikipedia.org/wiki/Android_NDK)
paudio   | Использовать нативный Paudio API AIX. [🔗](https://en.wikipedia.org/wiki/AIX)

###### Windows
Значение | Описание 
---------|----------
wasapi   | Использовать Windows Audio Session API. [🔗](https://docs.microsoft.com/en-us/windows/win32/coreaudio/wasapi)
dsound   | Использовать DirectSound API. [🔗](https://en.wikipedia.org/wiki/DirectSound)
winmm    | Использовать Windows Waveform Audio. [🔗](https://docs.microsoft.com/en-us/windows/win32/multimedia/waveform-audio)

###### Прочие
Значение | Описание 
---------|----------
naclaudio| Использовать Native Client. [🔗](https://en.wikipedia.org/wiki/Google_Native_Client)
haikuaudio | Использовать нативный API Haiku. [🔗](https://en.wikipedia.org/wiki/BeOS_API)
opensles | Использовать OpenSL ES API. [🔗](https://en.wikipedia.org/wiki/OpenSL_ES)
pspaudio | Использовать нативный API PlayStation Portable. [🔗](https://en.wikipedia.org/wiki/PlayStation_Portable)

###### Псевдо-устройства
Значение | Описание 
---------|----------
diskaudio| Запись на диск. [🔗](https://hg.libsdl.org/SDL/rev/ac6645260d31)
<a title="Эй, кукла. Ты задержан или просто глупый?">dummyaudio</a> | Отключить использование звукового API[.](https://youtu.be/tZuUNMwWhOU?t=61)
emscriptenaudio | Использовать Emscripten для браузера. [🔗](https://en.wikipedia.org/wiki/Emscripten)

#### SDL_PATH_DSP
Путь к устройству Open Sound System и Paudio. По-умолчанию `/dev/dsp`.

#### SDL_DSP_NOSELECT
Нужно ли использовать `SDL_IOReady()` для Paudio.

#### SDL_AUDIO_ALSA_DEBUG
Включить отладочный режим для ALSA.

#### SDL_AUDIO_FORMAT
Формат аудио.

#### SDL_AUDIO_FREQUENCY
Частота дискретизации.

#### SDL_AUDIO_CHANNELS
Количество каналов звука.

#### SDL_AUDIO_SAMPLES
Количество бит в семпле (глубина звучания).

#### SDL_AUDIO_DEVICE_NAME
Название звукового устройства.

#### SDL_WAVE_CHUNK_LIMIT
Область данных.
