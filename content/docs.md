+++
title = "Документы"
draft = false
+++

* [Сборник формул](https://gitlab.com/Darkness9724/formulas/raw/master/formulas.pdf) – большой сборник формул по физике и астрономии.
* [Переменные окружения SDL](sdl-envvars)
* [Тесты производительности ЯП](pl-test)
* [ШГ](fonts)
* [Список и описание пакетов CTAN](ctan)