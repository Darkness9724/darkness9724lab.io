+++
title = "МГК стафф"
draft = false

[taxonomies]
#categories = ["Ссылки"]
#tags = ["mgq", "as", "ng"]
+++
* [Список спин-оффов и модов](/docs/spinoffs)
## Русское сообщество
* [Перевод Monster Girl Quest: NG+](https://gitlab.com/Darkness9724/mgq-ng-ru/)
* [Дискорд экстремального рашн фендома MGQ](https://discordapp.com/invite/xEEAa2w)
* [Старая группа ВК](https://vk.com/monstergirlquest)
* [Новая группа ВК](https://vk.com/mgq_rus)
* [Аск персонажей ВК](https://vk.com/mgqask)
## Англоязычное сообщество
* [Вики](https://monstergirlquest.fandom.com/wiki/MonsterGirlQuest_Wiki)
