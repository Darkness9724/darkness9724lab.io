+++
title = "Возобновление перевода NG+"
description = "Что-то я подзабил на этот блог. В общем, недавно было выпущено пять патчей к переводу, исправляющих кучу ошибок."
draft = false
date = 2021-12-28
slug = "mgq-ng-2"

[taxonomies]
#categories = ["Перевод"]
tags = ["mgq", "renpy", "ng+"]
+++
Итак, перевод NG+ был возобновлён.

Как я уже писал, заморожен он был в связи с прекращением поддержки Python 2, так что мне пришлось ждать пока не будет выпущен Ren'Py с Python 3. И наконец, этот момент настал. Ren'Py 8 вышел в альфу на гитхабе проекта. Пришлось, конечно, помаяться со сборкой, но теперь всё чики-пуки.

На этом всё. Напоследок скриншоты:

<img src="/mgq-ng-2/Screenshot_20211228_4.webp"></img>
<img src="/mgq-ng-2/Screenshot_20211228_5.webp"></img>
<img src="/mgq-ng-2/Screenshot_20211228_6.webp"></img>
<img src="/mgq-ng-2/Screenshot_20211228_7.webp"></img>
<img src="/mgq-ng-2/Screenshot_20211228_9.webp"></img>
<img src="/mgq-ng-2/Screenshot_20211228_14.webp"></img>
<img src="/mgq-ng-2/Screenshot_20211228_19.webp"></img>
